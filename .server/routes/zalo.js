var express = require('express');
var router = express.Router();
const axios = require('axios').default;

const zsConfig = {
    appId: "3183433962987357424",
    redirectUri: 'https://vng-test-02-2020.firebaseapp.com/zalo_callback',
    appSecret: 'klfPMoL3XCEWMRW4G35Q'
}
/* GET users listing. */
router.get('/user_info', function (req, res, next) {

    const code = req.query['code'];

    console.log(req.query);

    if (code !== undefined) {
        axios.get(`https://oauth.zaloapp.com/v3/access_token?app_id=${zsConfig.appId}&app_secret=${zsConfig.appSecret}&code=${code}`)
            .then(z_res => {
                console.log(z_res.data);
                
                if (z_res.data['access_token'] !== undefined) {
                    axios.get(`https://graph.zalo.me/v2.0/me?access_token=${z_res.data['access_token']}&fields=id,birthday,name,gender,picture`)
                    .then(
                        user_info => {
                            res.json({
                                ...user_info.data,
                                access_token: z_res.data['access_token']
                            });
                        }
                    )
                    .catch( 
                        err => res.json({})
                        )
                } else {
                    res.json({})
                }
            })
            .catch( error =>{
                console.log(error);
                res.json({})
                
            });
    } else {
        res.json({})
    }
});

module.exports = router;