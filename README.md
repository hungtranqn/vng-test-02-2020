# VNG Test 022020


## Note 
- This project just run debug server
- Node version >= 8
## Run server proxy

- Make sure port 3000 is free

```
    cd .server
    npm install
    npm start
```


## Run client
Open new terminal
- Make sure port 8100 is free

```
    npm install 
    npm start 
    npm run start
```

## Test App

- [Open this link](https://localhost:8100)

- Click  `Advanced` 

![alt text](media/https.png)

- ...
