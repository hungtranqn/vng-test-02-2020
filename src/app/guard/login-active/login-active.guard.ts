import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';
import { MenuController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoginActiveGuard implements CanActivate {
  constructor(
    private storage: Storage,
    private menuController: MenuController,
    private router: Router
  ) {

  }
  async canActivate() {
    try {
      let userCredential = await this.storage.get(KeyStore.USER_CREDENTIAL);

      if (userCredential !== null && userCredential !== undefined) {
        this.menuController.enable(true);
        return true;
      } else {
        this.menuController.enable(false);
        this.router.navigateByUrl('/login');
        return false;
      }
    } catch (error) {
      this.router.navigateByUrl('/login');
      this.menuController.enable(false);
      return false;
    }


  }

}
