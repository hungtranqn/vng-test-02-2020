import { Component, OnInit } from '@angular/core';
import { MenuController, Platform, NavParams, NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ZaloLoginService } from 'src/app/services/login/zalo-login/zalo-login.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';
import { Router } from '@angular/router';
import { MainControllerService } from 'src/app/services/main-controller/main-controller.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private menuController: MenuController,
    private platform: Platform,
    private zaloLoginService: ZaloLoginService,
    private inAppBrowser: InAppBrowser,
    private storage: Storage,
    private router: Router,
    private mainControllerService: MainControllerService
  ) { }

  async ngOnInit() {
  }
  
  async ionViewWillEnter(){
    this.menuController.enable(false);
    await this.mainControllerService.checkLogined();  
    
  }
  async checkLogined(){
    let userCredential = await this.storage.get(KeyStore.USER_CREDENTIAL);
    console.log(userCredential);
    
    if(userCredential !== null && userCredential !== undefined){

      await this.router.navigateByUrl('/home');
    }

  }
  zaloLogin(){
    if(this.platform.is("cordova")){

    } else {
      let link = this.zaloLoginService.zaloClient.getLoginUrl();
      this.inAppBrowser.create(link,"_self");
    }
  }



  
}
