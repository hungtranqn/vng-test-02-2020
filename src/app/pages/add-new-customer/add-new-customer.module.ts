import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNewCustomerPageRoutingModule } from './add-new-customer-routing.module';

import { AddNewCustomerPage } from './add-new-customer.page';

import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddNewCustomerPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddNewCustomerPage]
})
export class AddNewCustomerPageModule {}
