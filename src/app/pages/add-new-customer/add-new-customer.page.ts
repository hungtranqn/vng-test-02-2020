import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, LoadingController } from '@ionic/angular';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { CustomerType } from 'src/app/interfaces/CustomerType';
import { Customer } from 'src/app/interfaces/Customer';
import { Status } from 'src/app/interfaces/Status';
import { Gender } from 'src/app/interfaces/Gender';
import { DatabaseService } from 'src/app/services/firebase/database/database.service';
@Component({
  selector: 'app-add-new-customer',
  templateUrl: './add-new-customer.page.html',
  styleUrls: ['./add-new-customer.page.scss'],
})
export class AddNewCustomerPage implements OnInit {

  types = [
    {
      value: CustomerType.KH_MOI,
      name: "New Customer"
    },
    {
      value: CustomerType.KH_THUONG,
      name: "Normal Customer"
    },
    {
      value: CustomerType.KH_THAN_THIET,
      name: "Loyal Customer"
    },
    {
      value: CustomerType.KH_VIP,
      name: "Vip Customer"
    }
  ];
  statues = [
    {
      value: Status.ACTIVE,
      name: "Active"
    },
    {
      value: Status.INACTIVE,
      name: "InActive"
    }
  ];
  genders = [
    {
      value: Gender.Male,
      name: "Male"
    },
    {
      value: Gender.Female,
      name: "Female"
    },
    {
      value: Gender.Other,
      name: "Other"
    },

  ]
  customerForm = this.formBuilder.group({
    customer_id: [
      '',
      Validators.required
    ],
    customer_name: [
      '',
      Validators.required
    ],
    customer_type: [
      CustomerType.KH_MOI,
      Validators.required
    ],
    balance: [
      0,
      Validators.required
    ],
    phone: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
      ])
    ],
    email: [
      '',
      Validators.required
    ],
    address: [
      '',
      Validators.required
    ],
    status: [
      Status.INACTIVE,
      Validators.required
    ],
    acount_number: [
      '',
      Validators.required
    ],
    gender: [
      Gender.Male,
      Validators.required
    ]
  });
  props = {
    role: "ADD",
    data: {
      docId: '',
      customer: {}
    }
  };
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private databaseService: DatabaseService,
    private loadingController: LoadingController
  ) { }


  async ngOnInit() {
    this.props = await this.navParams.get('props');
    if (this.props.role === "MODIFY") {
      this.pasteModify();
    }
  }

  pasteModify() {
    const customer: Customer = <Customer>this.props.data.customer;
    this.customerForm.setValue(customer);
  }

  async addNew() {
    await (await this.loadingController.create()).present();
    const customer = <Customer>this.customerForm.value;

    console.log(customer);
    const idDoc = this.databaseService.addNewCustomer(customer);
    await this.loadingController.dismiss();
    await this.modalController.dismiss();
  }

  async modify(){
    await (await this.loadingController.create()).present();
    const customer = <Customer>this.customerForm.value;
    this.databaseService.modifyCustomer(this.props.data.docId,customer);
    await this.loadingController.dismiss();
    await this.modalController.dismiss();
  }

  async cancel() {
    await this.modalController.dismiss();
  }

}
