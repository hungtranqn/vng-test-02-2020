import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ZaloCallbackPage } from './zalo-callback.page';

describe('ZaloCallbackPage', () => {
  let component: ZaloCallbackPage;
  let fixture: ComponentFixture<ZaloCallbackPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZaloCallbackPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ZaloCallbackPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
