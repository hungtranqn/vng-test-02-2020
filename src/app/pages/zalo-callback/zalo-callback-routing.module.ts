import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZaloCallbackPage } from './zalo-callback.page';

const routes: Routes = [
  {
    path: '',
    component: ZaloCallbackPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZaloCallbackPageRoutingModule {}
