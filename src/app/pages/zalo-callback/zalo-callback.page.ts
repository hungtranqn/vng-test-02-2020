import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ZaloLoginService } from 'src/app/services/login/zalo-login/zalo-login.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import * as firebase from 'firebase';
import { ProxyService } from 'src/app/services/proxy/proxy.service';
import { AuthService } from 'src/app/services/firebase/auth/auth.service';
import { DatabaseService } from 'src/app/services/firebase/database/database.service';
import { ToastController, MenuController } from '@ionic/angular';
import { MainControllerService } from 'src/app/services/main-controller/main-controller.service';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';

@Component({
  selector: 'app-zalo-callback',
  templateUrl: './zalo-callback.page.html',
  styleUrls: ['./zalo-callback.page.scss'],
})
export class ZaloCallbackPage implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private proxyService: ProxyService,
    private authService: AuthService,
    private databaseService: DatabaseService,
    private toastController: ToastController,
    private menuController: MenuController,
    private mainControllerService: MainControllerService,
    private storage: Storage
  ) {
    this.menuController.enable(false);
  }

  ngOnInit() {
    console.log("LOGIN");

    this.activatedRoute.queryParams.subscribe(async params => {

      // console.log(params);
      if (params['code'] != undefined) {

        try {
          let userInfo = await this.proxyService.getZaloUserInfo(params['code']);

          let userCredential = await this.authService.zaloUserLogin(userInfo['id']);
          // this.storage.set("efelnf", userCredential);
          if (userCredential.additionalUserInfo.isNewUser === true) {
            await this.databaseService.saveZaloUserInfo(userCredential.user.uid, userInfo)
          }
          this.mainControllerService.loginHandle();
        } catch (error) {
          await (await this.toastController.create({
            message: "Login fail!!!!!!!!!",
            duration: 3000
          })).present();

          this.router.navigateByUrl('login');
        }

      } else {
        this.router.navigateByUrl('login');
      }
    });
  }

}
