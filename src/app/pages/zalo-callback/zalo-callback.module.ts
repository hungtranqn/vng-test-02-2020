import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZaloCallbackPageRoutingModule } from './zalo-callback-routing.module';

import { ZaloCallbackPage } from './zalo-callback.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ZaloCallbackPageRoutingModule
  ],
  declarations: [ZaloCallbackPage]
})
export class ZaloCallbackPageModule {}
