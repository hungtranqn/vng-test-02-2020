import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { AddNewCustomerPage } from '../../add-new-customer/add-new-customer.page';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { FirebaseCustomer } from 'src/app/interfaces/FirebaseCustomer';
import { SortSelect } from 'src/app/interfaces/SortSelect';
import { SortType } from 'src/app/interfaces/SortType';
import { DatabaseService } from 'src/app/services/firebase/database/database.service';
import * as jspdf from 'jspdf';
import 'jspdf-autotable';
import { autoTable as AutoTable } from 'jspdf-autotable'


@Component({
  selector: 'app-customer',
  templateUrl: './customer.page.html',
  styleUrls: ['./customer.page.scss'],
})
export class CustomerPage implements OnInit {
  @ViewChild('pdfTable', null) pdfTable: ElementRef;

  readonly IN = "IN";
  readonly DE = "DE";

  tableHead = [
    {
      name: "ID",
      lable: "Id"
    },
    {
      name: "NAME",
      lable: "Name"
    },
    {
      name: "TYPE",
      lable: "Type"
    },
    {
      name: "BALANCE",
      lable: "Balance"
    },
    {
      name: "PHONE",
      lable: "Phone"
    },
    {
      name: "EMAIL",
      lable: "Email"
    },
    {
      name: "ADDRESS",
      lable: "Address"
    },
    {
      name: "STATUS",
      lable: "Status"
    },
    {
      name: "ACCOUNT_NUMBER",
      lable: "Account number"
    },
    {
      name: "GENDER",
      lable: "Gender"
    },

  ]
  firebaseCustomers: FirebaseCustomer[] = [];
  searchResult: FirebaseCustomer[] = [];
  idsSelected: Set<String> = new Set();
  sortSelect = SortSelect.ID;
  sortType = this.IN;

  textSearch = "";

  constructor(
    private modalController: ModalController,
    private customerService: CustomerService,
    private alertController: AlertController,
    private databaseService: DatabaseService
  ) { }



  async ngOnInit() {
    await this.customerService.initial();

    this.customerService.listen().subscribe(
      () => {
        setTimeout(() => {
          this.firebaseCustomers = [];
          this.customerService.firebaseCustomersMap.forEach((customer, docId) => {
            this.firebaseCustomers.push({
              docId: docId,
              customer: customer
            })
          });
          this.pipeSort();
          this.pipeSearch();
        }, 50);
      }
    )

  }



  async addNewCustomer() {
    await (await this.modalController.create({
      component: AddNewCustomerPage,
      componentProps: {
        props: {
          role: "ADD",
          data: {}
        }
      },
      cssClass: 'add-new-customer'
    })).present();
  }

  async modify(firebaseCustomer: FirebaseCustomer) {
    await (await this.modalController.create({
      component: AddNewCustomerPage,
      componentProps: {
        props: {
          role: "MODIFY",
          data: firebaseCustomer
        }
      },
      cssClass: 'add-new-customer'
    })).present();
  }

  async delete(firebaseCustomer: FirebaseCustomer) {
    await (await this.alertController.create({
      header: 'Are you sure, delete it?',
      buttons: [
        {
          text: "OK",
          role: 'ok',
          handler: async () => {
            await this.databaseService.deleteCustomer(firebaseCustomer.docId);
            this.idsSelected.delete(firebaseCustomer.docId);
          }
        },
        {
          text: "Cancel",
          role: 'cancel',
          handler: () => {
            console.log("Cancel");
          }
        }
      ]
    })).present();
  }

  changeItem($event: CustomEvent, firebaseCustomer: FirebaseCustomer) {
    if ($event.detail.checked === true) {
      this.idsSelected.add(firebaseCustomer.docId);
    } else {
      this.idsSelected.delete(firebaseCustomer.docId);
    }
  }
  checkSelect(firebaseCustomer: FirebaseCustomer) {
    return this.idsSelected.has(firebaseCustomer.docId)
  }

  selectAll($event: CustomEvent) {
    if ($event.detail.checked === true) {
      this.firebaseCustomers.forEach(element => {
        this.idsSelected.add(element.docId);
      });
    } else {
      this.idsSelected.clear();
    }
  }

  async exportSelected() {

    let data = this.searchResult.filter(firebaseCustomer => {
      return this.idsSelected.has(firebaseCustomer.docId);
    });

    if (data.length === 0) {
      await (await this.alertController.create({
        header: "Please select at least one customer !",
        buttons: ["OK"]
      })).present();
      return;
    }
    let csv = "Id,Name,Type,Balance,Phone,Email,Address,Status,Account number,Gender\n";

    data.forEach(firebaseCustomer => {
      csv += '"' + firebaseCustomer.customer.customer_id + '"' + ","
        + '"' + firebaseCustomer.customer.customer_name + '"' + ","
        + '"' + firebaseCustomer.customer.customer_type + '"' + ","
        + '"' + firebaseCustomer.customer.balance + '"' + ","
        + '"' + firebaseCustomer.customer.phone + '"' + ","
        + '"' + firebaseCustomer.customer.email + '"' + ","
        + '"' + firebaseCustomer.customer.address + '"' + ","
        + '"' + firebaseCustomer.customer.status + '"' + ","
        + '"' + firebaseCustomer.customer.acount_number + '"' + ","
        + '"' + firebaseCustomer.customer.gender + '"'
        + "\n";
    });
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'customer.csv';
    hiddenElement.click();

  }

  exportAll() {
    let csv = "Id,Name,Type,Balance,Phone,Email,Address,Status,Account number,Gender\n";

    this.firebaseCustomers.forEach(firebaseCustomer => {
      csv += '"' + firebaseCustomer.customer.customer_id + '"' + ","
        + '"' + firebaseCustomer.customer.customer_name + '"' + ","
        + '"' + firebaseCustomer.customer.customer_type + '"' + ","
        + '"' + firebaseCustomer.customer.balance + '"' + ","
        + '"' + firebaseCustomer.customer.phone + '"' + ","
        + '"' + firebaseCustomer.customer.email + '"' + ","
        + '"' + firebaseCustomer.customer.address + '"' + ","
        + '"' + firebaseCustomer.customer.status + '"' + ","
        + '"' + firebaseCustomer.customer.acount_number + '"' + ","
        + '"' + firebaseCustomer.customer.gender + '"'
        + "\n";
    });
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download = 'customer.csv';
    hiddenElement.click();
  }


  pdfExport() {
    const doc = new jspdf();
    doc.internal.pageSize.height = 1000;
    doc.internal.pageSize.width = 500;

    const head = [['Id', 'Name', 'Type', 'Balance', 'Phone', 'Email', 'Address', 'Status', 'Account number', 'Gender']]
    let data = [];

    this.firebaseCustomers.forEach(firebaseCustomer => {
      data.push([
        firebaseCustomer.customer.customer_id,
        firebaseCustomer.customer.customer_name,
        firebaseCustomer.customer.customer_type,
        firebaseCustomer.customer.balance,
        firebaseCustomer.customer.phone,
        firebaseCustomer.customer.email,
        firebaseCustomer.customer.address,
        firebaseCustomer.customer.status,
        firebaseCustomer.customer.acount_number,
        firebaseCustomer.customer.gender
      ])
    });
    ((doc as any).autoTable as AutoTable)({
      head: head,
      body: data,
      didDrawCell: data => {
        // console.log(data.column.index)
      },
    })
    doc.save('customer.pdf');
  }

  async pdfExportSelected() {
    let data = this.searchResult.filter(firebaseCustomer => {
      return this.idsSelected.has(firebaseCustomer.docId);
    });

    if (data.length === 0) {
      await (await this.alertController.create({
        header: "Please select at least one customer !",
        buttons: ["OK"]
      })).present();
      return;
    }

    const doc = new jspdf();
    doc.internal.pageSize.height = 1000;
    doc.internal.pageSize.width = 500;

    const head = [['Id', 'Name', 'Type', 'Balance', 'Phone', 'Email', 'Address', 'Status', 'Account number', 'Gender']]
    let pdfData = [];

    data.forEach(firebaseCustomer => {
      pdfData.push([
        firebaseCustomer.customer.customer_id,
        firebaseCustomer.customer.customer_name,
        firebaseCustomer.customer.customer_type,
        firebaseCustomer.customer.balance,
        firebaseCustomer.customer.phone,
        firebaseCustomer.customer.email,
        firebaseCustomer.customer.address,
        firebaseCustomer.customer.status,
        firebaseCustomer.customer.acount_number,
        firebaseCustomer.customer.gender
      ])
    });
    ((doc as any).autoTable as AutoTable)({
      head: head,
      body: pdfData
    })
    doc.save('customer.pdf')
  }

  toggleSort() {
    if (this.sortType === this.IN) {
      this.sortType = this.DE;
    } else {
      this.sortType = this.IN;
    }
  }
  chooseSort(name) {
    if (this.sortSelect === name) {
      this.toggleSort();
    } else {
      this.sortSelect = name;
      this.sortType = this.IN;
    }
    this.pipeSort();
    this.pipeSearch();
  }

  pipeSort() {
    if (this.sortSelect == SortSelect.ID) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_id > b.customer.customer_id) {
            return -1;
          } else if (a.customer.customer_id < b.customer.customer_id) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_id > b.customer.customer_id) {
            return 1;
          } else if (a.customer.customer_id < b.customer.customer_id) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.NAME) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_name > b.customer.customer_name) {
            return -1;
          } else if (a.customer.customer_name < b.customer.customer_name) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_name > b.customer.customer_name) {
            return 1;
          } else if (a.customer.customer_name < b.customer.customer_name) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.TYPE) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_type > b.customer.customer_type) {
            return -1;
          } else if (a.customer.customer_type < b.customer.customer_type) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.customer_type > b.customer.customer_type) {
            return 1;
          } else if (a.customer.customer_type < b.customer.customer_type) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.BALANCE) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.balance > b.customer.balance) {
            return -1;
          } else if (a.customer.balance < b.customer.balance) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.balance > b.customer.balance) {
            return 1;
          } else if (a.customer.balance < b.customer.balance) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.PHONE) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.phone > b.customer.phone) {
            return -1;
          } else if (a.customer.phone < b.customer.phone) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.phone > b.customer.phone) {
            return 1;
          } else if (a.customer.phone < b.customer.phone) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.EMAIL) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.email > b.customer.email) {
            return -1;
          } else if (a.customer.email < b.customer.email) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.email > b.customer.email) {
            return 1;
          } else if (a.customer.email < b.customer.email) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.ADDRESS) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.address > b.customer.address) {
            return -1;
          } else if (a.customer.address < b.customer.address) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.address > b.customer.address) {
            return 1;
          } else if (a.customer.address < b.customer.address) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.STATUS) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.status > b.customer.status) {
            return -1;
          } else if (a.customer.status < b.customer.status) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.status > b.customer.status) {
            return 1;
          } else if (a.customer.status < b.customer.status) {
            return -1;
          } else {
            return 0;
          }
        })
      } 
    } else if (this.sortSelect == SortSelect.ACCOUNT_NUMBER) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.acount_number > b.customer.acount_number) {
            return -1;
          } else if (a.customer.acount_number < b.customer.acount_number) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.acount_number > b.customer.acount_number) {
            return 1;
          } else if (a.customer.acount_number < b.customer.acount_number) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    } else if (this.sortSelect == SortSelect.GENDER) {
      if (this.sortType == this.DE) {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.gender > b.customer.gender) {
            return -1;
          } else if (a.customer.gender < b.customer.gender) {
            return 1;
          } else {
            return 0;
          }
        })
      } else {
        this.firebaseCustomers = this.firebaseCustomers.sort((a, b) => {
          if (a.customer.gender > b.customer.gender) {
            return 1;
          } else if (a.customer.gender < b.customer.gender) {
            return -1;
          } else {
            return 0;
          }
        })
      }
    }
  }

  searchChange($event){
    this.textSearch = (<string> $event['detail']['value']).toLowerCase();
    this.pipeSearch();
  }
  pipeSearch(){
    if(this.textSearch.trim() == ""){
      this.searchResult = this.firebaseCustomers;
    } else {
      this.searchResult = this.firebaseCustomers.filter(firebaseCustomer =>{
        return firebaseCustomer.customer.customer_id.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.customer_name.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.customer_type.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.balance.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.phone.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.email.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.address.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.status.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.acount_number.toString().toLowerCase().includes(this.textSearch)
              || firebaseCustomer.customer.gender.toString().toLowerCase().includes(this.textSearch);
      });
    }
  }
}
