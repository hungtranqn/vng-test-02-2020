import { Component, OnInit } from '@angular/core';
// import { Chart } from 'canvasjs';
import * as CanvasJS from 'src/assets/js/canvasjs.min';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { FirebaseCustomer } from 'src/app/interfaces/FirebaseCustomer';
import { CustomerType } from 'src/app/interfaces/CustomerType';
import { Gender } from 'src/app/interfaces/Gender';

@Component({
  selector: 'app-charts-graphs',
  templateUrl: './charts-graphs.page.html',
  styleUrls: ['./charts-graphs.page.scss'],
})
export class ChartsGraphsPage implements OnInit {
  firebaseCustomers: FirebaseCustomer[] = [];
  readonly TYPE_COUNT_INITAL = {
    "new": {
      y: 0,
      label: "New"
    },
    "normal": {
      y: 0,
      label: "Normal"
    },
    "loyal": {
      y: 0,
      label: "Loyal"
    },
    "vip": {
      y: 0,
      label: "Vip"
    }
  };
  readonly BALANCE_COUNT_INITAL = {
    "less_10M": {
      y: 0,
      label: " < 10 million"
    },
    "10M_100M": {
      y: 0,
      label: "10 million - 100 million"
    },
    "100M_1B": {
      y: 0,
      label: "100 million - 1 billion"
    },
    "1b_more": {
      y: 0,
      label: " > 1 billion"
    }
  };
  readonly GENDER_COUNT_INITAL = {
    "male": {
      y: 0,
      name: "Male"
    },
    "female": {
      y: 0,
      name: "Female"
    },
    "other": {
      y: 0,
      name: "Other"
    }
  };

  typeCount = this.TYPE_COUNT_INITAL;
  balanceCount = this.BALANCE_COUNT_INITAL;
  genderCount = this.GENDER_COUNT_INITAL;

  constructor(
    private customerService: CustomerService
  ) { }


  async ngOnInit() {
    await this.customerService.initial();
    this.customerService.listen().subscribe(
      () => {
        setTimeout(() => {
          this.firebaseCustomers = [];
          this.typeCount = {
            "new": {
              y: 0,
              label: "New"
            },
            "normal": {
              y: 0,
              label: "Normal"
            },
            "loyal": {
              y: 0,
              label: "Loyal"
            },
            "vip": {
              y: 0,
              label: "Vip"
            }
          };
          this.balanceCount = {
            "less_10M": {
              y: 0,
              label: " < 10 million"
            },
            "10M_100M": {
              y: 0,
              label: "10 million - 100 million"
            },
            "100M_1B": {
              y: 0,
              label: "100 million - 1 billion"
            },
            "1b_more": {
              y: 0,
              label: " > 1 billion"
            }
          };
          this.genderCount = {
            "male": {
              y: 0,
              name: "Male"
            },
            "female": {
              y: 0,
              name: "Female"
            },
            "other": {
              y: 0,
              name: "Other"
            }
          };
          this.customerService.firebaseCustomersMap.forEach((customer, docId) => {
            // type
            if (customer.customer_type === CustomerType.KH_MOI) {
              this.typeCount.new.y += 1;
            } else if (customer.customer_type === CustomerType.KH_THUONG) {
              this.typeCount.normal.y += 1;
            } else if (customer.customer_type === CustomerType.KH_THAN_THIET) {
              this.typeCount.loyal.y += 1;
            } else if (customer.customer_type === CustomerType.KH_VIP) {
              this.typeCount.vip.y += 1;
            }
            // balance
            if (customer.balance >= 0 && customer.balance < 10000000) {
              this.balanceCount["less_10M"].y += 1;
            } else if (customer.balance >= 10000000 && customer.balance < 100000000) {
              this.balanceCount["10M_100M"].y += 1;
            } else if (customer.balance >= 100000000 && customer.balance < 1000000000) {
              this.balanceCount["100M_1B"].y += 1;
            } else if (customer.balance >= 1000000000) {
              this.balanceCount["1b_more"].y += 1;
            }
            // gender
            if (customer.gender === Gender.Male) {
              this.genderCount["male"].y += 1;
            } else if (customer.gender === Gender.Female) {
              this.genderCount["female"].y += 1;
            } else if (customer.gender === Gender.Other) {
              this.genderCount["other"].y += 1;
            }

          });
          let typeChart = new CanvasJS.Chart("balanceContaner", {
            animationEnabled: true,
            exportEnabled: true,
            title: {
              text: "Balance"
            },
            data: [{
              type: "column",
              dataPoints: [
                this.balanceCount["less_10M"],
                this.balanceCount["10M_100M"],
                this.balanceCount["100M_1B"],
                this.balanceCount["1b_more"]]
            }]
          });

          let balanceChart = new CanvasJS.Chart("typeContaner", {
            animationEnabled: true,
            exportEnabled: true,
            title: {
              text: "Customer type"
            },
            data: [{
              type: "column",
              dataPoints: [
                this.typeCount.new,
                this.typeCount.normal,
                this.typeCount.loyal,
                this.typeCount.vip]
            }]
          });
          let genderChart = new CanvasJS.Chart("genderContaner", {
            theme: "light2",
            animationEnabled: true,
            exportEnabled: true,
            title: {
              text: "Gender"
            },
            data: [{
              type: "pie",
              showInLegend: true,
              toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
              indexLabel: "{name} - #percent%",
              dataPoints: [
                this.genderCount.male,
                this.genderCount.female,
                this.genderCount.other
              ]
            }]
          });


          typeChart.render();
          balanceChart.render();
          genderChart.render();
        }, 50);

      }
    )




  }

}
