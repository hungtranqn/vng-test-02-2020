import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChartsGraphsPage } from './charts-graphs.page';

describe('ChartsGraphsPage', () => {
  let component: ChartsGraphsPage;
  let fixture: ComponentFixture<ChartsGraphsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsGraphsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChartsGraphsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
