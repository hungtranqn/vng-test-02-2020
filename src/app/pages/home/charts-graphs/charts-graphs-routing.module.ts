import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChartsGraphsPage } from './charts-graphs.page';

const routes: Routes = [
  {
    path: '',
    component: ChartsGraphsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChartsGraphsPageRoutingModule {}
