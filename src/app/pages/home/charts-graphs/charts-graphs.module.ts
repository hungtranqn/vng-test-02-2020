import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChartsGraphsPageRoutingModule } from './charts-graphs-routing.module';

import { ChartsGraphsPage } from './charts-graphs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChartsGraphsPageRoutingModule
  ],
  declarations: [ChartsGraphsPage]
})
export class ChartsGraphsPageModule {}
