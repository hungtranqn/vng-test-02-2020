import { Component, OnInit } from '@angular/core';
import { UserInfoService } from 'src/app/services/user-info/user-info.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.page.html',
  styleUrls: ['./user-info.page.scss'],
})
export class UserInfoPage implements OnInit {

  userInfo: any = null;
  constructor(
    private userInfoService: UserInfoService
  ) { }

  async ngOnInit() {
    this.userInfo = await this.userInfoService.getZaloInfo();    
  }

}
