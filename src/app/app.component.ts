import { Component, OnInit } from '@angular/core';

import { Platform, MenuController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MainControllerService } from './services/main-controller/main-controller.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Customers',
      url: '/home/customer',
      icon: 'people'
    },
    {
      title: 'Charts & Graphs',
      url: '/home/charts-graphs',
      icon: 'bar-chart'
    },
    {
      title: 'User info',
      url: '/home/user-info',
      icon: 'person-circle'
    },

  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private mainControllerService: MainControllerService,
    private menuController: MenuController,
    private alertController: AlertController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.platform.is("cordova")) {
        this.statusBar.styleDefault();
        this.splashScreen.hide();
      }
    });
    this.mainControllerService.initial();
    this.menuController.enable(false);
  }

  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }

  }

  async logout() {
    await (await this.alertController.create({
      header: "Are you sure logout ?",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel logout");
          }
        },
        {
          text: "OK",
          role: "ok",
          handler: async () => {
            await this.mainControllerService.logoutHandle();
          }
        }
      ]
    })).present();
  }

}
