import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private storage: Storage
  ) { }


  zaloUserLogin(id): Promise<firebase.auth.UserCredential> {
    return new Promise(async (resolve, reject) => {
      try {
        const fakeEmail = `zalo${id}@gmail.com`;
        let listInposible = await firebase.auth().fetchSignInMethodsForEmail(fakeEmail);

        if (listInposible.length === 0) {
          firebase.auth().createUserWithEmailAndPassword(`zalo${id}@gmail.com`, id)
            .then(
              async userCredential => {
                console.log(userCredential);
                await this.storage.set(KeyStore.USER_CREDENTIAL,JSON.stringify(userCredential));
                resolve(userCredential);
              }
            )
        } else {
          firebase.auth().signInWithEmailAndPassword(`zalo${id}@gmail.com`, id)
            .then(
              async userCredential => {
                console.log(userCredential);
                await this.storage.set(KeyStore.USER_CREDENTIAL,JSON.stringify(userCredential));
                resolve(userCredential);
              }
            )
        }
      } catch (error) {
        console.log("Error");
        reject();

      }
    })
  }
}
