import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Customer } from 'src/app/interfaces/Customer';
import { FirebaseCustomer } from 'src/app/interfaces/FirebaseCustomer';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  constructor(
    private angularFirestore: AngularFirestore
  ) { }


  async saveZaloUserInfo(uid, data) {

    await firebase.firestore().collection('users').doc(uid).set({
      type: 'zalo',
      info: {
        ...data
      }
    });
  }


  getZaloUserInfo(uid: string): Promise<any> {
    return new Promise((resolve) => {
      firebase.firestore().collection('users').doc(uid).get()
        .then(
          result => {
            resolve(result.data()['info']);
          }
        )
    });
  }

  getAllCustomer(): Promise<FirebaseCustomer[]> {

    return new Promise((resolve) => {
      const firebaseCustomer = [];

      firebase.firestore().collection('customer').get().then(
        result => {
          result.docs.forEach(data => {
            firebaseCustomer.push({
              docId: data.id,
              customer: data.data()
            });
          });

          resolve(firebaseCustomer);
        }
      )
    })

  }

  addNewCustomer(customer: Customer): Promise<string> {
    return new Promise((resolve) => {
      firebase.firestore().collection('customer').add(customer)
        .then(
          response => {
            resolve(response.id);
          }
        );
    })
  }
  modifyCustomer(docId: string,customer: Customer): Promise<string> {
    return new Promise((resolve) => {
      firebase.firestore().collection('customer').doc(docId).update(customer)
        .then(
          response => {
            console.log(response);
            
          }
        );
    })
  }
  deleteCustomer(docId: string){
    return firebase.firestore().collection('customer').doc(docId).delete();
  }
}
