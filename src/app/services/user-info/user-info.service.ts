import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';

import * as firebase from 'firebase';
import { LoginService } from '../login/login.service';
import { DatabaseService } from '../firebase/database/database.service';
@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  zaloInfo: any = null;
  constructor(
    private loginService: LoginService,
    private databaseService: DatabaseService
  ) { 

  }

  async getZaloInfo(){
    let userCredential = await this.loginService.getUserCredential();
    return this.databaseService.getZaloUserInfo(userCredential.user.uid);
  }
}
