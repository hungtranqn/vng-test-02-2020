import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private storage: Storage
  ) { }

  async getUserCredential(){
    return <firebase.auth.UserCredential>JSON.parse(await this.storage.get(KeyStore.USER_CREDENTIAL));
  }

}
