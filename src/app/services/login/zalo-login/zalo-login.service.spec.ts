import { TestBed } from '@angular/core/testing';

import { ZaloLoginService } from './zalo-login.service';

describe('ZaloLoginService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ZaloLoginService = TestBed.get(ZaloLoginService);
    expect(service).toBeTruthy();
  });
});
