import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProxyService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getZaloUserInfo(code: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.httpClient.get(`${environment.proxy_server}/zalo/user_info?code=${code}`)
        .subscribe(
          res => {
            if (res['access_token'] !== undefined) {
              resolve(res);
            } else {
              reject();
            }
          },
          err => {
            reject();
          }

        )
    });

  }
}
