import { Injectable } from '@angular/core';
import { ZaloLoginService } from '../login/zalo-login/zalo-login.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { KeyStore } from 'src/app/constants/KeyStore';

@Injectable({
  providedIn: 'root'
})
export class MainControllerService {


  constructor(
    private zaloLoginService: ZaloLoginService,
    private router: Router,
    private storage: Storage,
  ) { }

  initial(){
    this.zaloLoginService.initial();
  }

  async checkLogined(){
    let userCredential = await this.storage.get(KeyStore.USER_CREDENTIAL);
    if(userCredential !== null && userCredential !== undefined){
      await this.router.navigateByUrl('/home');
    }
  }
  async loginHandle(){
    await this.router.navigateByUrl('/home');
  }

  async logoutHandle(){
    await this.storage.set(KeyStore.USER_CREDENTIAL, null);
    await this.router.navigateByUrl('/login');
  }

}
