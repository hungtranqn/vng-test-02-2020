import { Injectable } from '@angular/core';
import { FirebaseCustomer } from 'src/app/interfaces/FirebaseCustomer';
import * as firebase from 'firebase';
import { DatabaseService } from '../firebase/database/database.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Customer } from 'src/app/interfaces/Customer';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {


  firebaseCustomersMap: Map<string, Customer> = new Map();

  constructor(
    private databaseService: DatabaseService,
    private angularFirestore: AngularFirestore
  ) { }

  async initial() {
    this.getChange();
  }

  listen(){
    return this.angularFirestore.collection('customer').stateChanges();
  }
  getChange() {
    this.angularFirestore.collection('customer').stateChanges().subscribe(
      docsChange => {
        docsChange.forEach( //'added' | 'removed' | 'modified';
          docChange => {
            if (docChange.type == 'added') {
              this.firebaseCustomersMap.set(
                docChange.payload.doc.id,
                <Customer>docChange.payload.doc.data()
              )
            } else if (docChange.type == 'modified') {
              this.firebaseCustomersMap.set(
                docChange.payload.doc.id,
                <Customer>docChange.payload.doc.data()
              )
            } else {
              this.firebaseCustomersMap.delete(docChange.payload.doc.id);
            }
          }
        )

      }
    )
  }

}
