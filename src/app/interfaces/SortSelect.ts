export enum SortSelect{
    ID = "ID",
    NAME = "NAME",
    TYPE = "TYPE",
    BALANCE = "BALANCE",
    PHONE = "PHONE",
    EMAIL = "EMAIL",
    ADDRESS = "ADDRESS",
    STATUS = "STATUS",
    ACCOUNT_NUMBER = "ACCOUNT_NUMBER",
    GENDER = "GENDER"
}