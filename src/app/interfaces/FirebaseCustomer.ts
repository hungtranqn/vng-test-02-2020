import { Customer } from './Customer';

export interface FirebaseCustomer{
    docId: string,
    customer: Customer
}