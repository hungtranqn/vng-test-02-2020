export enum CustomerType{
    //A- khách hàng Vip, B - khách thàng thân thiết, C - Khách hàng thường, D - Khách hàng mới
    KH_VIP="Vip",
    KH_THAN_THIET = "Loyal",
    KH_THUONG = "Normal",
    KH_MOI = "New"
}