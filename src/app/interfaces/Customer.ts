import { CustomerType } from './CustomerType';
import { Gender } from './Gender';
import { Status } from './Status';

export interface Customer{
    customer_id: number,
    customer_name: string,
    customer_type: CustomerType,
    balance: number,
    phone: string,
    email: string,
    address: string,
    status: Status,
    acount_number: string,
    gender: Gender
}