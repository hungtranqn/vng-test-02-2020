// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDQ4oyr9itsRoT0BfJ_v5W-LUFUlB60Xt0",
    authDomain: "vng-test-02-2020.firebaseapp.com",
    databaseURL: "https://vng-test-02-2020.firebaseio.com",
    projectId: "vng-test-02-2020",
    storageBucket: "vng-test-02-2020.appspot.com",
    messagingSenderId: "878620408512",
    appId: "1:878620408512:web:ce34b577af8d70635c8b25",
    measurementId: "G-QEWNZ15JT4"
  },
  zsConfig: {
    appId: "3183433962987357424",
    redirectUri: 'https://localhost:8100/zalo_callback',
    appSecret: 'klfPMoL3XCEWMRW4G35Q'
  },
  proxy_server: 'http://localhost:3000'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
